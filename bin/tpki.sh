#!/bin/bash

set -euo pipefail
shopt -s expand_aliases

alias var='declare -r'
alias mut='declare'


ROOTDIR=$(
  pushd "`dirname $0`/.." >/dev/null
  echo $PWD
  popd >/dev/null
)


function group_dir
{
  var group=$1

  echo "$ROOTDIR/groups/$group"
}

function host_dir
{
  var host=$1

  echo "$ROOTDIR/hosts/$host"
}

function add_group
{
  var group=$1

  sudo /usr/sbin/groupadd tpki-$group &&
    mkdir "$(group_dir $group)" &&
    touch "$(group_dir $group)"/agent &&
    mkdir "$(group_dir $group)"/hosts
}

function drop_group
{
  var group=$1

  if [ -e "$(group_dir $group)" ]
  then
    show_group $group

    mut yn
    read -p "Are you sure you want to drop group '$group'? [yes/NO] " yn

    case $yn in
      yes)
        sudo /usr/sbin/groupdel tpki-$group &&
          rm -rf "$(group_dir $group)"

        echo "Dropped group '$group'"
        ;;
      *)
        echo "Aborting command 'drop-group $group'" >&2
        ;;
    esac
  else
    echo "Unknown group '$group'" >&2
  fi
  echo
}

function list_groups
{
  ls "`group_dir .`" |
    tr '\n' ' ' |
    sed -e 's/ $/\n/'
}

function show_group
{
  var group=$1

  var agent=$(cat "`group_dir $group`"/agent)
  var users=$(
    awk -F':' \
        "/^$group:/ {print \$4}" \
        /etc/group |
      sed -e 's/,/ /g'
  )
  var hosts=$(
    ls "`group_dir $group`"/hosts |
      tr '\n' ' ' |
      sed -e 's/ $/\n/'
  )

  echo "Group: $group"
  echo "Agent: $agent"
  echo "Users: $users"
  echo "Hosts: $hosts"
}

function show_groups
{
  var groups=$@

  for group in $groups
  do
    if [ -e "$(group_dir $group)" ]
    then
      show_group $group
    else
      echo "Unknown group '$group'" >&2
    fi
    echo
  done
}

function add_host
{
  var host=$1

  var new_host_dir=$(host_dir $host)

  if [ -e "$new_host_dir" ]
  then
    echo "Host '$host' already exists" >&2
  else
    mkdir "$new_host_dir"
    pushd "$new_host_dir" >/dev/null
    ssh-keygen -t rsa -b 2048 -f id_rsa -N ''
    popd >/dev/null
  fi
}

function drop_host
{
  var host=$1

  if [ -e "$(host_dir $host)" ]
  then
    var output=$(show_host $host)
    var groups=$(
      echo "$output" |
        awk '/^Groups:/ {$1=""; print}'
    )

    echo "$output"

    mut yn
    read -p "Are you sure you want to drop host '$host'? [yes/NO] " yn

    case $yn in
      yes)
        rm -rf "$(host_dir $host)"

        for group in $groups
        do
          rm "$(group_dir $group)"/hosts/$host
        done
        ;;
      *)
        echo "Aborting command 'drop-host $host'" >&2
        ;;
    esac
  else
    echo "Unknown host '$host'" >&2
  fi
  echo
}

function show_host
{
  var host=$1
  var host_path=$(host_dir $host)

  if [ -e "$host_path" ]
  then
    var groups=$(
      pushd "$(group_dir .)" >/dev/null

      find . -name $host |
        awk -F'/' '{print $2}' |
        tr '\n' ' ' |
        sed -e 's/ $/\n/'

      popd >/dev/null
    )
    
    echo "Host: $host"
    echo "Groups: $groups"
  else
    echo "Unknown host '$host'" >&2
  fi
  echo
}

function show_hosts
{
  var hosts=$@

  for host in $hosts
  do
    show_host $host
  done
}

function list_hosts
{
  ls "$(host_dir .)" |
    tr '\n' ' ' |
    sed -e 's/ $/\n/'
}

function join_host
{
  var host=$1
  var group=$2

  var hosts_path="$(group_dir $group)/hosts"

  if [ -e "$hosts_path/$host" ]
  then
    echo "Host '$host' is already joined to group '$group'" >&2
  else
    pushd "$hosts_path" >/dev/null

    ln -s "$(host_dir $host)"

    popd >/dev/null

    echo "Joined host '$host' to group '$group'" >&2
  fi
  echo
}

function part_host
{
  var host=$1
  var group=$2

  var hosts_path="$(group_dir $group)/hosts"

  if [ -e "$hosts_path/$host" ]
  then
    pushd "$hosts_path" >/dev/null

    rm $host

    popd >/dev/null

    echo "Parted host '$host' from group '$group'" >&2
  else
    echo "Host '$host' is not a member of group '$group'" >&2
  fi
  echo
}

function print_usage_and_exit
{
  exec 1>&2
  echo "Usage: $0 <command> [<arg> ...]"
  echo "  where"
  echo
  echo "  command :=  add-group"
  echo "           | drop-group"
  echo "           | show-groups"
  echo "           | list-groups"
  echo "           |  add-host"
  echo "           | drop-host"
  echo "           | show-hosts"
  echo "           | list-hosts"
  echo "           | join-host"
  echo "           | part-host"
  exit 1
}

function start_agents
{
  var groups=$(list_groups)

  for group in $groups
  do
    true
    #mut output=$(ssh-agent)
    # Store agent information under group
    # Set agent sock group
  done
}

function main
{
  var command=$1
  shift

  umask 0007
  chmod -R 770 "$ROOTDIR"

  case $command in
     add-group)     add_group  $@;;
    drop-group)    drop_group  $@;;
    show-groups)   show_groups $@;;
    list-groups)   list_groups $@;;
      add-host)     add_host   $@;;
     drop-host)    drop_host   $@;;
     show-hosts)   show_hosts  $@;;
     list-hosts)   list_hosts  $@;;
     join-host)    join_host   $@;;
     part-host)    part_host   $@;;
    start-agents) start_agents $@;;
    *) print_usage_and_exit    $@;;
  esac
}


if [ $# -eq 0 ]
then
  print_usage_and_exit
fi

main $@

